import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Post } from '../interfaces/post';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Version } from '../interfaces/version';
import { Profile } from '../interfaces/profile';

@Injectable()
export class ProfileService {
  constructor(private http: HttpClient) {
  }

  getProfiles(): Observable<Profile[]> {
    return this.http.get<Profile[]>(environment.BACKEND_URL + '/profile');
  }

  getVersions(id: number): Observable<Version[]> {
    return this.http.get<Version[]>(environment.BACKEND_URL + '/profile' + id + 'versions');
  }
}
