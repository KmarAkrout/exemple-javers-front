import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PostComponent } from './post/post.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomePageComponent } from './home-page/home-page.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { PostService } from './post/post.service';
import { MatTableModule, MatCheckboxModule, MatDialog, MatDialogModule, MatCardModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { ProfileComponent } from './profile/profile.component';
import { ProfileService } from './profile/profile.service';
import { UserComponent } from './user/user.component';
import { UserService } from './user/user.service';
import { VersionComponent } from './version/version.component';
import { VersionService } from './version/version.service';
import { VersionWithoutChildrenComponent } from './version-without-children/version-without-children.component';
import { VersionUserComponent } from './version-user/version-user.component';
import { VersionUserWithoutChildrenComponent } from './version-user-without-children/version-user-without-children.component';
import { CompareDialogComponent } from './compare-dialog/compare-dialog.component';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent,
    PostComponent,
    HomePageComponent,
    ProfileComponent,
    UserComponent,
    VersionComponent,
    VersionWithoutChildrenComponent,
    VersionUserComponent,
    VersionUserWithoutChildrenComponent,
    CompareDialogComponent
  ],
  imports: [
    BrowserModule, BrowserAnimationsModule, LayoutModule, MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule,
    AppRoutingModule, MatTableModule, MatCheckboxModule, HttpClientModule, MatDialogModule, MatCardModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
      closeButton: true
    })

  ],
  providers: [PostService, VersionService, HttpClientModule, ProfileService, UserService, PostComponent, VersionComponent],
  entryComponents: [
    CompareDialogComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
