import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { VersionService } from '../version/version.service';
import { ActivatedRoute } from '@angular/router';
import { Version } from '../interfaces/version';

@Component({
  selector: 'app-version-user',
  templateUrl: './version-user.component.html',
  styleUrls: ['./version-user.component.css']
})
export class VersionUserComponent implements OnInit {
  displayedColumns: string[] = ['version', 'author', 'createdAt','entity' ];
  versions: Version[] = [];
  dataSource;
  id;
  constructor(private versionService: VersionService , private route:ActivatedRoute) {
  }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getVersions(this.id);
  }
   

    getVersions(id) {
    this.versionService.getUserVersions(id).subscribe(data => {
      this.versions = data;
      this.dataSource = new MatTableDataSource(this.versions);
    });
  }
  

  
}