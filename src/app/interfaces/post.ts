import { PostComment } from './post-comment';

export interface Post {
  id: number;
  title: string;
  comments: PostComment [];
}
