import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Post } from '../interfaces/post';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Version } from '../interfaces/version';
import { Profile } from '../interfaces/profile';
import { User } from '../interfaces/user';

@Injectable()
export class UserService {
  constructor(private http: HttpClient) {
  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(environment.BACKEND_URL + '/user');
  }

  getVersions(id: number): Observable<Version[]> {
    return this.http.get<Version[]>(environment.BACKEND_URL + '/profile' + id + 'versions');
  }
}
