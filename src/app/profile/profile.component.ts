import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Profile } from '../interfaces/profile';
import { ProfileService } from './profile.service';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  displayedColumns: string[] = ['id', 'status', 'description','action'];
  profiles: Profile[] = [];
  dataSource;
  constructor(private profileService:ProfileService) {
  }

  ngOnInit() {
    this.getProfiles();
  }

  getProfiles() {
    this.profileService.getProfiles().subscribe(data => {
      this.profiles = data;
      this.dataSource = new MatTableDataSource(this.profiles);
    });

  }

}
