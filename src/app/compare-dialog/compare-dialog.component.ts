import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-compare-dialog',
  templateUrl: './compare-dialog.component.html',
  styleUrls: ['./compare-dialog.component.scss']
})
export class CompareDialogComponent implements OnInit {
  displayedColumns: string[] = ['propertyName', 'left', 'right' ];
  dataSource;
  constructor( public dialogRef: MatDialogRef<CompareDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.dataSource = this.data.comparaison;
  }

  ngOnInit() {
  }
  cancel() {
    this.dialogRef.close();
  }

}
