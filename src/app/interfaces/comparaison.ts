export interface Comparaison {
  propertyName: string;
  left: string;
  right: string;
}
