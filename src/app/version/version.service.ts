import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Version } from '../interfaces/version';
import { Comparaison } from '../interfaces/comparaison';

@Injectable()
export class VersionService {
  constructor(private http: HttpClient) {
  }

  getVersions(id: number): Observable<Version[]> {
    return this.http.get<Version[]>(environment.BACKEND_URL + '/post/versions/' + id);
  }

  getVersionsWithoutChildren(id: number): Observable<Version[]> {
    return this.http.get<Version[]>(environment.BACKEND_URL + '/post/versions/without/' + id);
  }

  getUserVersions(id: number): Observable<Version[]> {
    return this.http.get<Version[]>(environment.BACKEND_URL + '/user/versions/' + id);
  }

  getUserVersionsWithoutChildren(id: number): Observable<Version[]> {
    return this.http.get<Version[]>(environment.BACKEND_URL + '/user/versions/without/' + id);
  }

  compareVersions(id: number, firstVersionId: number, secondVersionId: number): Observable<Comparaison[]> {
    return this.http.get<Comparaison[]>
    (environment.BACKEND_URL + '/post/' + id + '/versions/diff?left='  + firstVersionId + '&right=' + secondVersionId);
  }

  compareTwoVersions(id: number, firstVersionId: number, secondVersionId: number): Observable<Comparaison[]> {
    return this.http.get<Comparaison[]>
    (environment.BACKEND_URL + '/post/versions/compare/' + id + '/' + firstVersionId + '/' + secondVersionId);
  }
}
