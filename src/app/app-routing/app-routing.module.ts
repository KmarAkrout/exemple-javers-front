import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from '../home-page/home-page.component';
import { PostComponent } from '../post/post.component';
import { ProfileComponent } from '../profile/profile.component';
import { UserComponent } from '../user/user.component';
import { VersionComponent } from '../version/version.component';
import { VersionWithoutChildrenComponent } from '../version-without-children/version-without-children.component';
import { VersionUserComponent } from '../version-user/version-user.component';
import { VersionUserWithoutChildrenComponent } from '../version-user-without-children/version-user-without-children.component';

const routes: Routes = [
  {
    path: '',
    component: HomePageComponent,
  },
  {
    path: 'post',
    component: PostComponent,
  },
  {
    path: 'user/versions/without/:id',
    component: VersionUserWithoutChildrenComponent
  },
  {
    path: 'user/versions/:id',
    component: VersionUserComponent
  },
  {
    path: 'post/versions/without/:id',
    component: VersionWithoutChildrenComponent
  },
  {
    path: 'post/versions/:id',
    component: VersionComponent
  },
  {
    path: 'user',
    component: UserComponent,
  },
  {
    path: 'profile',
    component: ProfileComponent,
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
