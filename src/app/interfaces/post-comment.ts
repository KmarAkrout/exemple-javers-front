export interface PostComment {
  id: number;
  review: string;
}
