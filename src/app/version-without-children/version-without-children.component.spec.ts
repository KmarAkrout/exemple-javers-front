import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VersionWithoutChildrenComponent } from './version-without-children.component';

describe('VersionWithoutChildrenComponent', () => {
  let component: VersionWithoutChildrenComponent;
  let fixture: ComponentFixture<VersionWithoutChildrenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VersionWithoutChildrenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VersionWithoutChildrenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
