import { Component, OnInit } from '@angular/core';
import { Version } from '../interfaces/version';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { VersionService } from '../version/version.service';
import { MatDialog } from '@angular/material';
import { CompareDialogComponent } from '../compare-dialog/compare-dialog.component';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-version-without-children',
  templateUrl: './version-without-children.component.html',
  styleUrls: ['./version-without-children.component.scss']
})
export class VersionWithoutChildrenComponent implements OnInit {
  displayedColumns: string[] = ['check', 'version', 'author', 'createdAt', 'entity' ];
  versions: Version[] = [];
  dataSource;
  id;
  checkedVersions: Version[] = [];
  constructor(private versionService: VersionService , private route: ActivatedRoute,
              public compareDialog: MatDialog, private toastr: ToastrService,
  ) {
  }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getVersionsWihoutchildren(this.id);
  }


   getVersionsWihoutchildren(id) {
    this.versionService.getVersionsWithoutChildren(this.id).subscribe(data => {
      this.versions = data;
      this.dataSource = new MatTableDataSource(this.versions);
    });
  }
  chooseVersion(event, version) {
    this.toastr.success('checked');
    if (!event.checked) {
      this.checkedVersions = this.checkedVersions.filter(element => element.version !== version.version);
    } else if (this.checkedVersions.length < 2) {
      this.checkedVersions.push(version);
    } else {

      this.toastr.error('vous ne pouvez pas selectionner plus que 2 versions');
    }
  }

  compare() {
    this.checkedVersions.sort((a, b) => (a.version > b.version) ? 1 : -1);
    this.versionService.compareVersions(this.id, this.checkedVersions[0].version, this.checkedVersions[1].version).subscribe(data => {
      const dialogRef = this.compareDialog.open(CompareDialogComponent, {
        width: '800px',
        data: {comparaison: data}
      });
      dialogRef.afterClosed().subscribe(result => {

      });

    });
  }
}
