import { Component, OnInit } from '@angular/core';
import { UserService } from './user.service';
import { Profile } from '../interfaces/profile';
import { User } from '../interfaces/user';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  displayedColumns: string[] = ['id','username','email','password','profile','actionWithChildren','actionWithoutChildren'];
  users: User[] = [];
  dataSource;
  constructor(private userService:UserService,private router:Router ) { }

  ngOnInit() {

    this.getUsers();
  }
  versionWithoutchildren(selectedId) 
  {
    this.router.navigate(['/user/versions/without/',selectedId.id])
  }

  version(selectedId)
  {
    this.router.navigate(['/user/versions/',selectedId.id])
  }
   getUsers() {
    this.userService.getUsers().subscribe(data=>{
      this.users = data ;
      this.dataSource=new MatTableDataSource(this.users);
    })
  }
}
