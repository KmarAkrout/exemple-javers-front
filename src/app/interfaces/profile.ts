
export interface Profile {
  id: number;
  status: string;
  description: string;
}
