import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VersionUserWithoutChildrenComponent } from './version-user-without-children.component';

describe('VersionUserWithoutChildrenComponent', () => {
  let component: VersionUserWithoutChildrenComponent;
  let fixture: ComponentFixture<VersionUserWithoutChildrenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VersionUserWithoutChildrenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VersionUserWithoutChildrenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
