import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { VersionService } from '../version/version.service';
import { ActivatedRoute } from '@angular/router';
import { Version } from '../interfaces/version';

@Component({
  selector: 'app-version-user-without-children',
  templateUrl: './version-user-without-children.component.html',
  styleUrls: ['./version-user-without-children.component.css']
})
export class VersionUserWithoutChildrenComponent implements OnInit {
  displayedColumns: string[] = ['check', 'version', 'author', 'createdAt', 'entity' ];
  versions: Version[] = [];
  dataSource;
  id;
  checkedVersions: Version[] = [];
  constructor(private versionService: VersionService , private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getVersions(this.id);
  }


    getVersions(id) {
    this.versionService.getUserVersionsWithoutChildren(id).subscribe(data => {
      this.versions = data;
      this.dataSource = new MatTableDataSource(this.versions);
    });
  }
  chooseVersion(event, version) {
    if (!event.checked) {
      this.checkedVersions = this.checkedVersions.filter(element => element.version !== version.version);
    } else if (this.checkedVersions.length < 2) {
      this.checkedVersions.push(version);
    }
  }

  compare() {
    this.checkedVersions.sort((a, b) => (a.version > b.version) ? 1 : -1);
    console.log(this.checkedVersions);
    this.versionService.compareVersions(this.id, this.checkedVersions[0].version, this.checkedVersions[1].version).subscribe(data => {
      console.log(data);
    });
  }


}
