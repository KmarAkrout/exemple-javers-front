import { Component, OnInit } from '@angular/core';
import { Post } from '../interfaces/post';
import { PostService } from './post.service';
import { MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { VersionComponent } from '../version/version.component';


@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  displayedColumns: string[] = ['id', 'title', 'actionWithChildren','actionWithoutChildren'];
  posts: Post[] = [];
  dataSource;



  constructor(private postService: PostService ,private router:Router ) {
  }

  ngOnInit() {
    this.getPosts();
  }

  versionWithoutchildren(selectedId) 
  {
    this.router.navigate(['/post/versions/without/',selectedId.id])
  }

  version(selectedId)
  {
    this.router.navigate(['/post/versions/',selectedId.id])
  }
  

  getPosts() {
    this.postService.getPosts().subscribe(data => {
      this.posts = data;
      this.dataSource = new MatTableDataSource(this.posts);
    });
  }
  
  


}
