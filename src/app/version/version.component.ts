import { Component, OnInit } from '@angular/core';
import { Version } from '../interfaces/version';
import { MatTableDataSource } from '@angular/material/table';
import { VersionService } from './version.service';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material/dialog';
import { CompareDialogComponent } from '../compare-dialog/compare-dialog.component';
import * as _ from 'lodash';


@Component({
  selector: 'app-version',
  templateUrl: './version.component.html',
  styleUrls: ['./version.component.scss']
})
export class VersionComponent implements OnInit {
  displayedColumns: string[] = ['check', 'version', 'author', 'createdAt', 'entity'];
  versions: Version[] = [];
  dataSource;
  id;
  checkedVersions: Version[] = [];

  constructor(private versionService: VersionService, private route: ActivatedRoute, private compareDialog: MatDialog, private toastr: ToastrService) {
  }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getVersions(this.id);
  }




  getVersions(id) {
     let index  = 1;
     this.versionService.getVersions(id).subscribe(data => {
      this.versions = data;
      // this.versions = _.uniqBy(this.versions, 'createdAt');
      this.versions.forEach(action => {
        action.version = index++;
      });
      this.dataSource = new MatTableDataSource(this.versions);
    });
  }

  chooseVersion(event, element) {
    if (!event.checked) {
      this.checkedVersions = this.checkedVersions.filter(e => e.version !== element.version);
    } else if (this.checkedVersions.length < 2) {
      element = this.versions[element.version - 1 ];
      console.log(element.version);
      this.checkedVersions.push(element);
    }
  }

  compare() {

    this.checkedVersions.sort((a, b) => (a.version > b.version) ? 1 : -1);
    this.versionService.compareTwoVersions(this.id, this.checkedVersions[0].version, this.checkedVersions[1].version).subscribe(data => {
      const dialogRef = this.compareDialog.open(CompareDialogComponent, {
        width: '800px',
        data: {comparaison: data}
      });
      dialogRef.afterClosed().subscribe(result => {

      });

    });

  }
}
