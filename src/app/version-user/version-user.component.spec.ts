import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VersionUserComponent } from './version-user.component';

describe('VersionUserComponent', () => {
  let component: VersionUserComponent;
  let fixture: ComponentFixture<VersionUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VersionUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VersionUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
