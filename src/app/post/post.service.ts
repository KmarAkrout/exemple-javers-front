import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Post } from '../interfaces/post';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Version } from '../interfaces/version';

@Injectable()
export class PostService {
  constructor(private http: HttpClient) {
  }

  getPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(environment.BACKEND_URL + '/post');
  }

  getVersions(id: number): Observable<Version[]> {
    return this.http.get<Version[]>(environment.BACKEND_URL + '/post/versions/' + id );
  }
  
}
