
export interface Version {
   version: number;
   author: string;
   createdAt: Date;
   updatedAt: Date;
   entity: any;
   isCurrentVersion: boolean;
}
